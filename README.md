# BTC Toy - Success !

Sent BTC to myself + 1500 satoshi fee on testnet

MyWallet: mribDJt5YfEVpauSvRUAxJMwEwPNKawCtK

TXID: dbc570521362afeaf350768236977bdb51163898d0774e1016c82d0e3f2f9293
UTXO index: 1

Transaction Hex:

010000000193922f3f0e2dc816104e77d098381651db7b9736827650f3eaaf62135270c5db010000006b48304502210084cb7bc817c723952cf6dbf7ef638eb001495f15fb31f405e90591861c4ab17b02201a815a57e43ab658becbc7a41605102bf9ea5fe4df9e2357d4bf6d3be34dc302012103694be48e2434a9474492fb5a303ba61e1845b98d5d82d2a0994aab4136880df6ffffffff02240d0000000000001976a91460d48c923f82eb94c3712fe581b387654ad22c2588ace7020000000000001976a9147adcc6e688e8cacaa84bd8cadb356a6345352b9388ac00000000

Broadcast Link:

https://live.blockcypher.com/btc-testnet/tx/8823649c279841d2cb40c4e80f56f538f796b9f6955275a1de992e16ccdaf97e/


## Setup

- Forker et cloner le repo
- Vous aurez besoin de python 3.11+. Il est déconseillé d'essayer d'upgrader son python système. Utiliser pyenv ou une machine virtuelle.
  - Linux: https://realpython.com/intro-to-pyenv/
  - Windows: https://pyenv-win.github.io/pyenv-win/
- Installer poetry 1.6+ https://python-poetry.org/docs/#installation
- Lancer `poetry install` dans votre repo cloné
- Lancer `source .bashrc` pour activer l'environnement virtuel
- Tenter d'executer les tests `pytest` qui devraient échouer à ce stade

## Workflow

- L'objectif du TP sera d'implémenter les exercices afin de faire passer les tests au fur à mesure.
- Toutes les commandes sont à lancer depuis la racine du repertoire
- Pour lancer des tests spécifiques:
  - `pytest tests/CHEMIN_VERS_FICHIER`
  - `pytest -k NOM_DU_TEST_OU_PARTIE_DU_NOM_DU_TEST`
- Pour afficher les prints lors de l'execution des tests ajouter l'option `-s`
- Pour afficher le nom des tests qui échouent ajouter l'option `-v`
